import requests
import time
import csv
import json
from .mwclient import MwQuery

def IntIfPoss(value):
	try:
		return int(value)
	except ValueError:
		return value

class WikiDataClient(object):

	# See also https://commons.wikimedia.org/wiki/Commons:Bots
	# Incomplete implementation since maxlag not implemented on some methods
	# Still some way to go before https://www.mediawiki.org/wiki/API:Client_code/Gold_standard

	def __init__(self, apiurl):
		self.apiurl = apiurl
		self.apiver = None
		self.session = requests.Session()

	def GetSiteInfo(self):

		dat = MwQuery(self.apiurl, self.apiver, self.session, action='query', 
			meta='siteinfo',
			siprop='general|namespaces')

		generator = dat['query']['general']['generator'] #something like MediaWiki 1.27.7
		splGen = generator.split(' ')
		if splGen[0] == "MediaWiki" and len(splGen) >= 2:
			verSplit = tuple(map(IntIfPoss, splGen[1].split('.')))
			self.apiver = verSplit

		return dat['query']

	def GetToken(self, tokenType="csrf", wait=None):
		kargs = {'type': tokenType, 'format': "json"}

		if wait is not None: time.sleep(wait)

		dat = MwQuery(self.apiurl, self.apiver, self.session, action='query',
			meta="tokens", 
			 **kargs)

		return dat['query']['tokens']

	def Login(self, lgname, lgpassword, lgtoken, wait=None):

		if wait is not None: time.sleep(wait)

		data = {
			'action':"login",
			'lgname': lgname,
			'lgpassword': lgpassword,
			'lgtoken': lgtoken,
			'format':"json"
		}
		print ("data", data)

		dat = self.session.post(self.apiurl, data=data)
		return (dat)

	def GetEntities(self, ids=None, wait=None):
	
		entities = {}
		kargs = {}
		if ids is not None: kargs['ids'] = '|'.join(map(str, ids))

		if wait is not None: time.sleep(wait)

		dat = MwQuery(self.apiurl, self.apiver, self.session, action='wbgetentities',
			 **kargs)

		for entityId in dat['entities']:
			entities[entityId] = dat['entities'][entityId]

		return entities

	def SetSiteLink(self, entityId, linksite, token=None, bot=True, wait=None, **kargs):

		if wait is not None: time.sleep(wait)

		data = {'action': 'wbsetsitelink',
			'linksite': linksite, 'format': 'json', 'token': token, 'bot': bot, **kargs}
		print (data)
		dat = self.session.post(self.apiurl, params={}, data=data)

		return dat


