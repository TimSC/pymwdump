import requests
import json
import argparse
import os
import time
from xml.sax import saxutils

def IntWherePoss(v):
	try:
		return int(v)
	except ValueError:
		return v

class MwApiClient(object):

	def __init__(self, apiurl, session=None, userAgent="python wikidataclient"):
		self.apiurl = apiurl
		self.apiver = None
		if session == None:
			session = requests.Session()
		self.session = session
		self.userAgent = userAgent

	def GetSiteInfo(self):

		dat = self._Query(meta='siteinfo', 
			siprop='general|namespaces')

		generator = dat['query']['general']['generator'] #something like MediaWiki 1.27.7
		splGen = generator.split(' ')
		if splGen[0] == "MediaWiki" and len(splGen) >= 2:
			verSplit = tuple(map(IntWherePoss, splGen[1].split('.')))
			self.apiver = verSplit

		return dat['query']

	def GetPageList(self, apnamespace=None, wait=None):

		apfrom = None
		pages = []

		while True:
			if wait is not None: time.sleep(wait)

			dat = self._Query(list='allpages', apfrom=apfrom, aplimit=500, apnamespace=apnamespace)
			pages.extend(dat['query']['allpages'])
			
			if 'continue' in dat:
				apfrom = dat['continue']['apcontinue']
			else:
				break
			
		return pages

	def GetPages(self, pageids=None, titles=None, rvlimit=None, wait=None, streamOut=None):
	
		rvcontinue = None
		pages = {}

		kargs = {}
		if pageids is not None: kargs['pageids'] = '|'.join(map(str, pageids))
		if titles is not None: kargs['titles'] = '|'.join(map(str, titles))
		if rvlimit is not None: kargs['rvlimit'] = rvlimit

		while True:
			if wait is not None: time.sleep(wait)

			if rvcontinue is not None:
				kargs['rvcontinue'] = rvcontinue

			if self.apiver is not None and self.apiver >= (1,32):
				kargs['rvslots'] = '*'

			dat = self._Query(prop='revisions', 
				rvprop='timestamp|user|userid|comment|content|flags|sha1|size',
				 **kargs)

			if streamOut is None:
				for pageId in dat['query']['pages']:
					page = dat['query']['pages'][pageId]
					if 'revisions' not in page: continue #Does the page exist?

					revs = page['revisions']
					if pageId not in pages:
						pages[pageId] = page
					else:
						pages[pageId]['revisions'].extend(revs)
			else:
				streamOut.Process(dat['query']['pages'])

			if 'continue' in dat:
				rvcontinue = dat['continue']['rvcontinue']
			else:
				break

		return pages

	def GetAllImages(self, ailimit=None, wait=None):
		#api.php?action=query&format=json&list=allimages&aifrom=Graffiti_000&ailimit=3 

		aicontinue = None
		images = []

		kargs = {'list': 'allimages'}
		if ailimit is not None: kargs['ailimit'] = ailimit

		while True:
			if wait is not None: time.sleep(wait)

			if aicontinue is not None:
				kargs['aicontinue'] = aicontinue

			dat = self._Query(**kargs)

			images.extend(dat['query']['allimages'])

			if 'continue' in dat:
				aicontinue = dat['continue']['aicontinue']
			else:
				break

		return images

	def GetCategories(self, wait=None, aclimit=10):

		acfrom = None
		cats = []

		while True:
			if wait is not None: time.sleep(wait)

			dat = self._Query(list='allcategories', acfrom=acfrom, aclimit=aclimit)
			cats.extend(dat['query']['allcategories'])

			if 'continue' in dat:
				acfrom = dat['continue']['accontinue']
			else:
				break
			
		return cats

	def GetCategoryMembers(self, cmtitle=None, cmpageid=None, cmtype=None, wait=None, cmlimit=10):

		cmstarthexsortkey = None
		pages = []

		while True:
			if wait is not None: time.sleep(wait)

			dat = self._Query(list='categorymembers', cmtitle=cmtitle, cmpageid=cmpageid, cmlimit=cmlimit, 
				cmstarthexsortkey=cmstarthexsortkey, cmtype=cmtype)
			pages.extend(dat['query']['categorymembers'])

			if 'continue' in dat:
				cmstarthexsortkey = dat['continue']['cmcontinue'].split("|")[1]
			else:
				break
			
		return pages

	def TextQuery(self, srsearch, srnamespace=None, wait=None, maxresults=None):

		sroffset = None
		out = {'search': []}
		queryParams = {
			'action': 'query',
			'list': 'search',
			'srsearch': srsearch,
		}
		if srnamespace is not None:
			queryParams['srnamespace'] = "|".join([str(sr) for sr in srnamespace])

		while True:
			if wait is not None: time.sleep(wait)
			if sroffset is not None:
				queryParams['sroffset'] = sroffset

			dat = self._Query(**queryParams)
			out['search'].extend(dat['query']['search'])
			out['totalhits'] = dat['query']['searchinfo']['totalhits']

			if maxresults is not None and len(out['search']) >= maxresults:
				out['search'] = out['search'][:maxresults]
				break

			if 'continue' in dat:
				sroffset = dat['continue']['sroffset']
			else:
				break
	
		return out

	def Geosearch(self, lat, lon, radius, gsprimary="all", gsnamespace=None, gslimit=None):

		queryParams = {
			'action': 'query',
			'list': 'geosearch',
			'gsprimary':gsprimary, 
			'gsradius':radius, 
			'gscoord':"{}|{}".format(lat, lon)
		}
		if gsnamespace is not None:
			queryParams['gsnamespace'] = "|".join([str(sr) for sr in gsnamespace])
		if gslimit is not None:
			queryParams['gslimit'] = gslimit

		dat = self._Query(**queryParams)
		return dat['query']['geosearch']

	def _Query(self, **kargs):

		queryParams = {
			"action": "query",
			'format': 'json', 
		}
		queryParams.update(kargs)
		for k in list(queryParams.keys()):
			if queryParams[k] == None:
				del queryParams[k]

		headers = {'User-Agent': self.userAgent}

		if self.apiver is not None and self.apiver >= (1, 29):
			errorformat = "plaintext"
			queryParams['errorformat'] = errorformat
		else:
			errorformat = "bc" #Backward compatible mode

		if self.apiver is None or self.apiver >= (1, 10):
			queryParams['maxlag'] = 5

		retryMaxlag = True
		dat = None

		while retryMaxlag:

			r = self.session.get(self.apiurl, headers=headers, params=queryParams)

			dat = r.json()
			if (r.status_code == 200 and 'errors' in dat and len(dat['errors']) > 0 
				and 'code' in dat['errors'][0] and dat['errors'][0]['code'] == "maxlag"):

				time.sleep(5)
			else:
				retryMaxlag = False

		if r.status_code != 200:
			raise RuntimeError("HTTP request failed {}".format(r.status_code))

		if 'warnings' in dat:
			if errorformat == 'plaintext':
				for warning in dat['warnings']:
					print ("Warning:", warning['*'])

			if errorformat == 'bc':
				for warning in dat['warnings']:
					print ("Warning:", dat['warnings'][warning])

		if 'errors' in dat:
			if errorformat == 'plaintext':
				for error in dat['errors'][1:]:
					print ("Error:", error['*'])
				firstError = dat['errors'][0]
				if firstError['code'] == 'badvalue':
					raise ValueError(firstError['*'])
				raise RuntimeError(firstError['*'])

			if errorformat == 'bc':
				for error in dat['error']:
					raise RuntimeError(dat['warnings'][warning])
					
		return dat


class HandlePageData(object):
	
	def __init__(self, generator):
		self.generator = generator
		self.inPage = False
		self.apiver = self.generator.apiver

	def Process(self, pages):

		page = list(pages.items())[0][1]

		if not self.inPage:
			self.generator.startElement("page", {})
			self.generator.startElement("title", {})
			self.generator.characters(page['title'])
			self.generator.endElement("title")
			self.generator.characters("\n")

			self.generator.startElement("ns", {})
			self.generator.characters(str(page['ns']))
			self.generator.endElement("ns")
			self.generator.characters("\n")

			self.generator.startElement("id", {})
			self.generator.characters(str(page['pageid']))
			self.generator.endElement("id")
			self.generator.characters("\n")

			#self.generator.startElement("restrictions", {})
			#<restrictions>edit=sysop:move=sysop</restrictions>
			#self.generator.endElement("restrictions")
			#self.generator.characters("\n")

			self.inPage = True

		revs = page['revisions']
		for rev in revs:

			if self.apiver is not None and self.apiver >= (1,32):
				mainSlot = rev['slots']['main'] #Used from 1.32
			else:
				mainSlot = rev #For older mediawiki <= 1.31
			#If text is hidden, so not process this revision, since the dump requires this data
			if 'texthidden' in mainSlot:
				continue

			self.generator.startElement("revision", {})

			self.generator.startElement("timestamp", {})
			self.generator.characters(rev['timestamp'])
			self.generator.endElement("timestamp")
			self.generator.characters("\n")

			self.generator.startElement("contributor", {})

			if 'user' in rev:
				if 'userid' not in rev or rev['userid'] != 0:
					self.generator.startElement("username", {})
					self.generator.characters(rev['user'])
					self.generator.endElement("username")
				else:
					self.generator.startElement("ip", {})
					self.generator.characters(rev['user'])
					self.generator.endElement("ip")

			if 'userid' in rev:
				self.generator.startElement("id", {})
				self.generator.characters(str(rev['userid']))
				self.generator.endElement("id")

			self.generator.endElement("contributor")
			self.generator.characters("\n")

			if 'comment' in rev:
				self.generator.startElement("comment", {})
				self.generator.characters(rev['comment'])
				self.generator.endElement("comment")
				self.generator.characters("\n")

			if 'contentmodel' in mainSlot:
				self.generator.startElement("model", {})
				self.generator.characters(mainSlot['contentmodel'])
				self.generator.endElement("model")
				self.generator.characters("\n")

			if 'contentformat' in mainSlot:
				self.generator.startElement("format", {})
				self.generator.characters(mainSlot['contentformat'])
				self.generator.endElement("format")
				self.generator.characters("\n")

			textAttrib = {}
			textAttrib['bytes']=str(rev['size'])
			if 'sha1' in rev:
				textAttrib['sha1']=rev['sha1'] 
			#xml:space="preserve"

			#Text is a required field in dump
			self.generator.startElement("text", textAttrib)
			self.generator.characters(mainSlot['*'])
			self.generator.endElement("text")
			self.generator.characters("\n")

			isMinor = 'minor' in rev
			if isMinor:
				self.generator.startElement("minor", {})
				self.generator.endElement("minor")
				self.generator.characters("\n")

			self.generator.endElement("revision")
			self.generator.characters("\n")

	def EndPage(self):
		self.generator.endElement("page")
		self.generator.characters("\n")
		self.inPage = False

def GetPageRevisions(client, generator, args, pageid=None, title=None):

	kargs = {'rvlimit': 'max'}
	if pageid is not None: kargs['pageids'] = [pageid]
	if title is not None: kargs['titles'] = [title]
	if args.curonly: kargs['rvlimit'] = 1

	handlePageData = HandlePageData(generator)
	client.GetPages(streamOut=handlePageData, wait=args.wait, **kargs)

	handlePageData.EndPage()



