pymwdump
========

A python 3 tool to dump mediawiki (1.27 and later, possibly earlier) sites using Action API calls. Inspired by dumpgenerator.py from https://github.com/WikiTeam/wikiteam I had a hard time trying to update that to modern python, so I wrote this tool from scratch. It depends on the requests module.

    pip3 install -r requirements.txt

    python3 pymwdump.py http://mywiki/api.php --images --xml

Instructions on how to restore the dump: https://www.mediawiki.org/wiki/Manual:Importing_XML_dumps

Other python libraries are available: https://www.mediawiki.org/wiki/API:Client_code

