import requests
import json
import argparse
import os
import time
from xml.sax import saxutils

from mwclient import MwApiClient, GetPageRevisions

def SaveImages(client, args):

	if not os.path.exists("images"):
		os.mkdir("images")

	imgList = client.GetAllImages(ailimit='max', wait=args.wait)
	print ("Found", len(imgList), "images")
	for i, img in enumerate(imgList):
		if args.wait is not None: time.sleep(args.wait)
		if i % 100 == 0:
			print ("Saving", i, "of", len(imgList), "images")

		r = requests.get(img['url'])

		if r.status_code == 404:
			print ("Detected missing file", img['url'])
			continue

		if r.status_code != 200:
			raise RuntimeError("HTTP request failed {}".format(r.status_code))

		outImg = open(os.path.join("images", img['name']), "wb")
		outImg.write(r.content)
		outImg.close()

def DumpWiki(args):

	client = MwApiClient(args.url)

	siteInfo = client.GetSiteInfo()

	if not args.resume:	
		nsList = siteInfo['namespaces']
		genInfo = siteInfo['general']
		#for k in genInfo:
		#	print (k, genInfo[k])
		print ("Found", len(nsList), "namespaces")

		pagesInNs = {}
		for nsId in nsList:
			ns = nsList[nsId]
			nsId = int(nsId)
			
			if nsId >= 0:
				pages = client.GetPageList(apnamespace=nsId, wait=args.wait)

				pagesInNs[nsId] = pages

		json.dump({'pagesInNs': pagesInNs, 'nsList': nsList, 
			'genInfo': genInfo}, open("pages.json", "wt"))
	else:
		dat = json.load(open("pages.json", "rt"))
		pagesInNs, nsList, genInfo = dat['pagesInNs'], dat['nsList'], dat['genInfo']

	if args.xml:
		history = open("history.xml", "wt")
		generator = saxutils.XMLGenerator(history, "utf-8")
		generator.apiver = client.apiver

		generator.startDocument()
		generator.startElement("mediawiki", {'xml:lang': genInfo['lang'],
			'xmlns': "http://www.mediawiki.org/xml/export-0.11/",
			'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
			'xsi:schemaLocation': "http://www.mediawiki.org/xml/export-0.11/ http://www.mediawiki.org/xml/export-0.11.xsd",
			'version': "0.11"})
		generator.characters("\n")

		generator.startElement("siteinfo", {})
		generator.characters("\n")

		generator.startElement("sitename", {})
		generator.characters(genInfo['sitename'])
		generator.endElement("sitename")
		generator.characters("\n")

		generator.startElement("base", {})
		generator.characters(genInfo['base'])
		generator.endElement("base")
		generator.characters("\n")

		generator.startElement("generator", {})
		generator.characters(genInfo['generator'])
		generator.endElement("generator")
		generator.characters("\n")
		
		generator.startElement("case", {})
		generator.characters(genInfo['case'])
		generator.endElement("case")
		generator.characters("\n")

		#<dbname>scubaw_wiki</dbname>

		generator.startElement("namespaces", {})
		generator.characters("\n")

		for nsId in nsList:
			ns = nsList[nsId]
			print (ns)
			generator.startElement("namespace", {'key': nsId, 'case': ns['case']})
			generator.characters(ns['*'])
			generator.endElement("namespace")
			generator.characters("\n")

		generator.endElement("namespaces")
		generator.characters("\n")

		generator.endElement("siteinfo")
		generator.characters("\n")
		
		for nsId in pagesInNs:

			pages = pagesInNs[nsId]
			for i, page in enumerate(pages):

				print (i, "of", len(pages), page)
				pageId = page['pageid']
		
				p = GetPageRevisions(client, generator, args, pageid=pageId)

		generator.endElement("mediawiki")
		generator.endDocument()
		history.close()

	if args.images:
		SaveImages(client, args)


if __name__=="__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument('url', type=str,
					help='url of api.php e.g. https://en.wikipedia.org/w/api.php')
	parser.add_argument('--xml', action='store_true', help='Export article pages')
	parser.add_argument('--resume', action='store_true', help='Continue previous dump')
	parser.add_argument('--curonly', action='store_true', help='Only get current version page articles')
	parser.add_argument('--images', action='store_true', help='Export images')
	parser.add_argument('--wait', type=float, help="Wait number of seconds between requests",
		default=None)

	args = parser.parse_args()

	DumpWiki(args)

